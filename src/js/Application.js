import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";
// `<progress class="progress is-small is-primary" max="100">0</progress>`

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }
  constructor(page) {
    super();
    this._loading=(function() {
      let bar=document.createElement("progress");
      bar.className="progress is-small is-primary"
      bar.setAttribute('max','100');
      console.log(bar);
      
      return bar;
    })();
    this.emit(Application.events.READY);
    this.on('ready',  async ()=>   this._create(page))
  }

  async _create(page) {
      this._startLoading() ;
      let response= await this._load(page);   
      let  planets=await response.json();
      this._stopLoading(planets)
  }

  _startLoading() {
    
    const box = document.createElement("div");
    box.classList.add("box");
    box.innerHTML = this._render({
      name: 'Placeholder',
      terrain: 'placeholder',
      population: 0,
    
    });
    document.body.querySelector(".main").appendChild(box);
    document.body.querySelector(".box").appendChild(this._loading);
   }

   
 
  _stopLoading(planets) {
    let mainEl=document.getElementsByClassName('main')[0]
  
     mainEl.innerHTML='';
    

    planets.results.map(x=>{
      const box = document.createElement("div");
      
        box.classList.add("box");
 
        box.innerHTML = this._render({
          name: x.name,
          terrain: x.terrain,
          population: x.population
        });
    
        document.body.querySelector(".main").appendChild(box);
       }
  
  
       );
  }
  async _load(page=''){

   let result= await fetch('https://swapi.boom.dev/api/planets'+page);
   
   return result;
     
  }
  
  _render({ name, terrain, population,loading2}) {
    console.log(loading2);
    return `
     
  ${loading2==undefined?`<progress style="visibility:hidden;" class="progress is-small is-primary" max="100">0</progress>`:loading2}
<article class="media">
  <div class="media-left">
    <figure class="image is-64x64">
      <img src="${image}" alt="planet">
    </figure>
  </div>
  <div class="media-content">
    <div class="content">
    <h4>${name}</h4>
      <p>
        <span class="tag">${terrain}</span> <span class="tag">${population}</span>
        <br>
      </p>
    </div>
  </div>
</article>
    `;
  }

  
}

