import "./scss/app.scss";
import Application from "./js/Application";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const app = new Application();
  const app2 = new Application('?page=2');
  const app3 = new Application('?page=3');
  const app4 = new Application('?page=4');
  const app5 = new Application('?page=5');
  const app6 = new Application('?page=6');

  // Used to access the app instance by the automated tests
  console.log(app._events.ready.fn());
  console.log(app2._events.ready.fn());
  console.log(app3._events.ready.fn());
  console.log(app4._events.ready.fn());
  console.log(app5._events.ready.fn());
  console.log(app6._events.ready.fn());
   

  window.__JS_APP = app2;
});