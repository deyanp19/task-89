// class declaration
// classes are templates for creating objects
class Rectangle {
    constructor(bombo, clock) {
        this.height=bombo;
        this.width=clock;
    }
}

const Rectangle2 = class {
    constructor(bombo,clock) {
        //this is what confused me!
        // this is inside function {} braces and we are told on line 2 thst this creates objects, but this is assigment below, so i made suddle mistake of thinking of this as object, but object syntax is ':' . { this.height:bombo} === this is object assignment.
        // so REMEMBER   constructor is a function. lets get the definition
        // constructor is a special method of a class for crating and initializing an object instance of that class.
        // description of constructor
        //constructor enebles you to provide any custom initialization that must be done before any other methods inside the classs can be used when the class is instantiated/ i think they mean created with `new` keyword.
        //FUNCTION INSTANTIATION PATTERN    
        // It creates an instance of an object through the use of a function. Convention is that this func starts with cappital letter /eg its a class/
        //ABOUT THE CLASS   
        // In the initializer, this refers to the class instance under construction, and super refers to the prorotype property of the base class, wihich contains the base class's instance methods, but not its instance fields. The new extended class contains only the methods but not the fields of the class it is extending.
        //SETTER INSIDE CONSTRUCTOR is executed when in the mother class there is a set declared function. But if you refere outside the constructor to this setter it just have the field.
        this.height=bombo;
        this.width=clock;
    }
    
}

 